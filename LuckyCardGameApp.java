public class LuckyCardGameApp {

    public static void main(String[] args) {
        GameManager manager = new GameManager();
        int totalPoints = 0;
        System.out.println(
                "Welcome Player! This game is called \"Lucky!\"." +
                        "Two cards are drawn from a deck. If your card " +
                        "matches the other card, you get points. If it doesn't, " +
                        "then you lose points. You must get 5 points or more " +
                        "before the deck runs out of cards. Good luck!");
        int gameRound = 0;
        while (manager.getNumberOfCards() > 1 && totalPoints < 5) {
            manager.dealCards();
            System.out.println(manager);
            totalPoints += manager.calculatePoints();
            System.out.println("\nTotal points: " + totalPoints);
            gameRound++;
        }
        System.out.println("################################\nGame round: " + gameRound);
        if (totalPoints < 5) {
            System.out.println("You have lost. Your final score was: " + totalPoints + ".");
        } else {
            System.out.println(
                    "You have won. Your total points were: " + totalPoints + ".\n" +
                            manager.getNumberOfCards() + " cards were left.");
        }
        System.out.println("Thanks for playing!");
    }
}