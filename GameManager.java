public class GameManager {
    private Deck drawPile;
    private Card centreCard;
    private Card playeCard;

    public GameManager() {
        System.out.println("\nCreating new game...");
        this.drawPile = new Deck();
        drawPile.shuffle();
    }

    public String toString() {
        return "\n--------------------------------\n" +
                "Centre Card: " + this.centreCard + "\n" +
                "Player Card: " + this.playeCard +
                "\n--------------------------------\n";
    }

    public void dealCards() {
        System.out.println("________________________________\n\nDealing cards...");
        drawPile.shuffle();
        this.centreCard = drawPile.drawTopCard();
        drawPile.shuffle();
        this.playeCard = drawPile.drawTopCard();
    }

    public int calculatePoints() {
        System.out.println("\nCalculating points...");
        if (centreCard.getRank() == playeCard.getRank())
            return 4;
        else if (centreCard.getSuit() == playeCard.getSuit())
            return 2;
        return -1;
    }

    public int getNumberOfCards() {
        return drawPile.getNumberOfCards();
    }
}