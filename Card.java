public class Card {
    private String suit;
    private int rank;

    public Card(String suit, int rank) {
        this.suit = suit;
        this.rank = rank;
    }

    public String getSuit() {
        return this.suit;
    }

    public int getRank() {
        return this.rank;
    }

    public String toString() {
        String nameOfRank = Integer.toString(rank);
        switch (this.rank) {
            case (1):
                nameOfRank = "Ace";
                break;
            case (11):
                nameOfRank = "Jack";
                break;
            case (12):
                nameOfRank = "Queen";
                break;
            case (13):
                nameOfRank = "King";
                break;
        }
        return nameOfRank + " of " + this.suit;
    }
}
