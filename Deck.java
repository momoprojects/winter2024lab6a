import java.util.Random;

public class Deck {
    private Card[] cards;
    private int numOfCards;
    private Random rng;

    public Deck() {
        System.out.println("\nCreating new deck...\n");
        this.numOfCards = 52;
        rng = new Random();
        cards = new Card[this.numOfCards];
        String[] listOfSuits = { "Spades", "Hearts", "Diamonds", "Clubs" };
        int[] listOfRanks = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
        int i = 0;
        for (String s : listOfSuits) {
            for (int r : listOfRanks) {
                cards[i] = new Card(s, r);
                i++;
            }
        }
    }

    public String toString() {
        // another way to print entire array
        // return Arrays.deepToString(cards);
        System.out.println("\nPrinting deck...");
        String output = "";
        for (int i = 0; i < this.numOfCards; i++) {
            output += cards[i] + "\n";
        }
        return output;
    }

    public int deckLength() {
        return this.numOfCards;
    }

    public Card drawTopCard() {
        this.numOfCards--;
        Card lastCard = cards[numOfCards];
        cards[numOfCards] = null;
        return lastCard;
    }

    public void shuffle() {
        for (int i = 0; i < this.numOfCards; i++) {
            Card temp = this.cards[i];
            int randomIndex = rng.nextInt(this.numOfCards);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }

    public int getNumberOfCards() {
        return this.numOfCards;
    }

    public void removeFromNumOfCards(int numberOfCardsToBeTaken) {
        this.numOfCards -= numberOfCardsToBeTaken;
    }

}